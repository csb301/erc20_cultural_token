// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract CulturalToken is ERC20 {
    address owner;
    string public name = "Culture Incentive Token";
    string public description = "A Token of incentive for host";
    string public symbol = "CulT";
    uint8 public decimals = 2;
    uint256 public INITIAL_SUPPLY = 1000000;

    constructor() public {
        _mint(msg.sender, INITIAL_SUPPLY);
        owner = msg.sender;
    }

    // Event to log hosted cultural events
    event CulturalEventHosted(
        address hostAddress,
        string title,
        string place,
        uint256 date,
        string reason
    );

    function hostCulturalEvent(
        address hostAddress,
        string memory title,
        string memory place,
        uint256 date,
        string memory reason
    ) public {
        // Transfer 20 tokens to the event host
        _transfer(owner, hostAddress, 20);

        // Emit an event to log the hosted cultural event
        // emit CulturalEventHosted(hostAddress, title, place, date, reason);
    }
}
